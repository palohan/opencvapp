#include <cstdlib>
#include <cstdarg>
#include <array>

#include <cstdio>
#include <iostream>


#define _USE_MATH_DEFINES
#include <math.h>

#include <opencv2/opencv.hpp>

namespace rockets
{
    constexpr auto g = 9.80665;
    constexpr auto TARGET_CLICK = -1;
    constexpr auto X_OFFSET = 50;

    constexpr auto winWidthMax = 1000;
    constexpr auto winWidthMin = 600;

    constexpr auto winHeightMax = 600;
    constexpr auto winHeightMin = 300;

    inline auto radToDeg(double rad) { return 180 / M_PI * rad; }
    inline auto degToRad(double deg) { return M_PI / 180 * deg; }

    using namespace std;
    using namespace cv;

    inline double sinalfa(double v0, double alfa);
    inline double cosalfa(double v0, double alfa);
    inline double fallTime(double v0y);
    inline double Y_timePoint(double v0y, double t);
    inline double Y_speed(double v0y, double t);
    inline double X_speed(double v0x);
    inline double X_timePoint(double v0x, double t);
    inline double squareDist(double x, double y);
    inline double uhol(double y, double x);

    void mouseHandler(int event, int x, int y, int flags, void *param);

    int tarPos, clickControl = TARGET_CLICK, thisLap;


    cv::Mat setImg, playImg, setImgCopy;                                     //opencv veci
    cv::Mat *pSetImg = &setImg, *pPlayImg = &playImg, *pSetImgCopy = &setImgCopy;

    struct CvFont
    {
        int face;
        double scale;
        int thickness;
        int line_type;
    };

    const CvFont
        MEDIUM_FONT = {FONT_HERSHEY_PLAIN, 0.7, 1, LINE_AA},
        SMALL_FONT = {FONT_HERSHEY_PLAIN, 1, 1, LINE_AA},
        BIG_FONT = {FONT_HERSHEY_PLAIN, 1.2, 2, LINE_AA};

    FILE *outFile = nullptr, *inputFile = nullptr;


    const cv::Scalar COLORS[] =
    {
        {0, 0, 255},                       //(b, g, r)         //COLOR_RED
        {0, 255, 0},                                           //COLOR_GREEN
        {0, 255, 255},                                         //COLOR_YELLOW
        {255, 0, 0},                                           //COLOR_BLUE
        {255, 255, 0},                                         //COLOR_LIGHTBLUE
        {255, 0, 255},                                         //COLOR_PINK
        {255, 255, 255}                                        //COLOR_WHITE
    };

    enum colorNames{RED, GREEN, YELLOW, BLUE, LIGHTBLUE, PINK, WHITE};


    void dualPrintf(FILE* stream, const char* format, ...)
    {
        va_list args, args2;
        va_start(args, format);
        va_copy(args2, args);

        vprintf(format, args);                                      //stdout

        if (stream)
            vfprintf(stream, format, args2);                         //file

        va_end(args);
        va_end(args2);
    }


    template <typename T1, typename T2, typename T3> bool numberIsBetween(T1 dist, T2 lower, T3 upper)
    {
        return lower < dist && dist <= upper;
    }

    template < typename T> void arrPointers(T *z, int p)
    {
         for (int i = 0; i < p; i++)
             cout << &z[i] << endl;

         puts("");
    }


    void printPoints(const char* name1, const cv::Point& point1, const char* name2, const cv::Point& point2)
    {
        printf("%s: %d %d, %s: %d %d\n", name1, point1.x, point1.y, name2, point2.x, point2.y);
    }

    template < typename T1, typename T2> void printWhat(const char* name1, T1 what1, const char* name2, T2 what2)
    {
        cout << name1 << ": " << what1 << ", " << name2 << ": " << what2 << endl;
    }

    void printCompare(bool c)
    {
        printf("%d\n", c);
    }


    void flushLine(FILE* stream = stdin)
    {
        while (fgetc(stream) != '\n');
    }

    void flushAll(FILE* stream = stdin)
    {
#ifdef _WIN32
        fflush(stream);                                    //win
#elif 1
        char c;
        while ((c = fgetc(stream)) != '\n' && c != EOF);
#elif 0
       __fpurge(stdin);                                  //lin
#endif
    }

    bool isWhiteSpaceString(const char* str)
    {
        for (; *str; str++)
            if (!isblank(*str))                              //pri 1. znaku co neni medzera vieme, ze neni cely biely
                return false;

        return true;
    }

	template<class T1, class T2>
    double pointDistance(const Point_<T1>& p1, const Point_<T2>& p2)
    {
        return squareDist(p1.x - p2.x, p1.y - p2.y);
    }


    struct Line
    {
        cv::Point A, B;

        //vyraz Point2d p_A -> tvorba praznej premenej => potrebuje empty ctor
        Line() = default;

        Line(const cv::Point& A, const cv::Point& B) : A(A), B(B) {}
    };


    class SmartString
    {
    private:
        cv::Mat* m_pImg = nullptr;
        string m_str;
        const CvFont* m_font = nullptr;

    public:
        cv::Size m_size;
        cv::Point m_point;
        cv::Scalar m_color;

		SmartString() = default;

        SmartString(cv::Mat* p_i, const CvFont* f, const string& str = {}) : m_pImg(p_i), m_font(f)
        {
            setText(str);
        }

        SmartString(cv::Mat* p_i, const CvFont* f, cv::Scalar c, const string& str = {}) : m_pImg(p_i), m_font(f), m_color(c)
        {
            setText(str);
        }

        SmartString(const SmartString& smartString) :
            m_pImg(smartString.m_pImg), m_font(smartString.m_font), m_point(smartString.m_point), m_color(smartString.m_color)
        {
            setText(smartString.m_str);
        }

        SmartString& operator =(const SmartString& other)
        {
            if (this == &other)
                return *this;

            m_pImg = other.m_pImg;
            m_font = other.m_font;
            m_point = other.m_point;
            m_color = other.m_color;

            setText(other.m_str);

            return *this;
        }

        void setNumber(int number)
        {
            std::string m_str = std::to_string(number);
            setText(m_str.data());
        }

        void setText(const string& str)
        {
            m_str = str;

            int baseLine = 0;
            m_size = cv::getTextSize(m_str, m_font->face, m_font->scale, m_font->thickness, &baseLine);
        }

        void setCenterPoint(int X_pos, int Y_pos)
        {
            m_point.x = X_pos - m_size.width / 2;
            m_point.y = Y_pos - m_size.height / 2;
        }

        void display(const cv::Point& p, const cv::Scalar& c)         //urci bod a farbu
        {
            _cvPutText(*m_pImg, p, c);
        }

        void display(const cv::Scalar& c, cv::Mat* img = nullptr)                    //urci farbu
        {
            Mat& mat = img ? *img : *m_pImg;

            _cvPutText(mat, m_point, c);
        }

        void display(const cv::Point& p, cv::Mat* img = nullptr)                    //urci bod
        {
            Mat& mat = img ? *img : *m_pImg;

            _cvPutText(mat, p, m_color);
        }

        void display()                              //urci farbu
        {
            _cvPutText(*m_pImg, m_point, m_color);
        }

    private:
        void _cvPutText(const cv::Mat& img, const cv::Point& point, const cv::Scalar& color)
        {
            cv::putText(img, m_str, point, m_font->face, m_font->scale, color, m_font->thickness, m_font->line_type);
        }
    };


    class Target
    {
    private:
        cv::Scalar m_colorDefault = {0}, m_colorCurrent = {0};

        Line m_whiteLine;

        SmartString m_label;


    public:
        cv::Point m_screenPoint;                     //bod na okne
        cv::Point m_mathPoint;                        //logicky bod
        int m_size;                                 //velkost terca

        cv::Mat* m_pImg = nullptr;                      //smernik na vec, kde je adresa obrazku

        Target() = default;


        Target(cv::Mat* pImg,
               int x_pos,                          //pozicia v okne
               int size, const cv::Scalar& color, const CvFont* font) :

            m_colorDefault(color),
            m_label(pImg, font, m_colorDefault),
            m_mathPoint(x_pos, 0),                        //matematicky bod
            m_size(size),
            m_pImg(pImg)
        {
            m_label.setNumber(x_pos);

            m_screenPoint.x = x_pos + X_OFFSET;                       //bod na obrazovke rakiet - preto offset
            m_label.m_point.x = m_screenPoint.x - m_label.m_size.width / 2;
        }


        void setScreenPoint(int rocketScreenHeight)
        {
            m_screenPoint.y = rocketScreenHeight;
            m_label.m_point.y = m_screenPoint.y + 15 + m_label.m_size.height / 2;

            m_whiteLine = Line(cv::Point(m_label.m_point.x, m_screenPoint.y), cv::Point(m_label.m_point.x + m_label.m_size.width, m_screenPoint.y));
        }

        bool isHit(const Point2d& point) const
        {
            return pointDistance(point, m_mathPoint) <= m_size;
        }

        void draw()
        {
            draw(m_colorDefault);
        }

        void draw(const cv::Scalar& color)
        {
            if (m_colorCurrent == color)              //ak uz ma terc tuto farbu, nic sa nerobi
                return;

            m_colorCurrent = color;                               //toto je nova sucasna farba

            cv::circle(*m_pImg, m_screenPoint, m_size, m_colorCurrent, FILLED);

            /*prekryje label terca a jeho dolnu polku.
                *horny lavy  bod: X = lavy okraj labela; Y = dolna polka kruhu
                *dolny pravy bod: X = pravy okraj labela; Y = spodok labelu */
            cv::rectangle(*m_pImg, cv::Point(m_whiteLine.A.x, m_screenPoint.y + 1), cv::Point(m_whiteLine.B.x, m_label.m_point.y), cv::Scalar(0), FILLED);

            cv::line(*m_pImg, m_whiteLine.A, m_whiteLine.B, COLORS[WHITE]);                   //doplni, co premazal obdlznik

            m_label.display(color);
        }
    };


    class Player
    {
    private:
           char
           clickString[100],                    //info po kliku: uhol, rychlost...
           paramString[100],                    //veliciny letu
           flightString[50];                    //kde dopadol / ci zasiahol


           double
           m_dSpeed,
           m_dRiseTime,
           m_dFallSpeedX, m_dFallSpeedY, m_dFallSpeed;

           short math_y;

           cv::Point m_playerHitPoint, m_rocketHitPoint;

        public:
           char
           playerName[11],                      //meno hraca (unikatne)
           rocketName[15],                                    //meno rakety
           allString[400],
           resultString[55];

           SmartString smartPlayerName, smartRocketName;


           short
			   m_nTotalHits = 0,
			   m_nTotalShots = 1,                        //pocetnost mien. samotny vyskyt => totalShots = 1
			   m_nLapHits = 0,                           //zasahy iba za TOTO kolo
			   m_nPlayerNameLabelHeight,
			   m_nRocketId,
			   m_nPlayerId;

           double m_dAngle,
           m_dFlightTime, m_dFlightLength, m_dFlightHeight,
           v0x, v0y,
           m_dHitTime = 0;

           bool hit = false,                                //ci trafil ciel
           ownsName = true;


           cv::Scalar color;
           cv::Point linePoint;

           Player* nameOwner = this;                        //defaultne je hrac majitelom svojho mena


           static int
           s_nRocketId,
           s_nPlayerId,
           &s_nWinHeight,                               //vyska okna
           s_nMaxRocketNameWidth,
           s_nMaxHitCountWidth,
           s_nPlayersCount;

           static Size2d& playWinSize;


           Player() :
               smartPlayerName(pPlayImg, &MEDIUM_FONT),
               smartRocketName(pPlayImg, &MEDIUM_FONT),

               m_nRocketId(++s_nRocketId),
               m_nPlayerId(++s_nPlayerId)
           {}

           void calcParams(cv::Point point)
           {
               linePoint = point;

               math_y = s_nWinHeight - linePoint.y;

               m_dSpeed = squareDist(linePoint.x, math_y);           //dlzka spojnice bodov [0, 0] a [x, math_y]

               m_dAngle = uhol(math_y, linePoint.x);               //atan medzi Y a X suradnicou koncoveho bodu strely

               v0y = sinalfa(m_dSpeed, m_dAngle);
               v0x = cosalfa(m_dSpeed, m_dAngle);
               m_dFlightTime = fallTime(v0y);
               m_dRiseTime = m_dFlightTime / 2;
               m_dFlightLength = X_timePoint(v0x, m_dFlightTime);
               m_dFlightHeight = Y_timePoint(v0y, m_dRiseTime);
               m_dFallSpeedX = X_speed(v0x);
               m_dFallSpeedY = Y_speed(0, m_dRiseTime);
               m_dFallSpeed = squareDist(m_dFallSpeedX, m_dFallSpeedY);
           }


           Point2d timePoint(double time)
           {
               return Point2d(X_timePoint(v0x, time), Y_timePoint(v0y, time));
           }

           cv::Point flightPoint(double time)
           {
               Point2d Tpoint = timePoint(time);

               return cv::Point(X_OFFSET + Tpoint.x, playWinSize.height - Tpoint.y);
           }

           Line flightLine(double time)
           {
               return Line(flightPoint(time), flightPoint(time + 0.1));
           }

           void setRocketNamePoint()
           {
               smartRocketName.m_point = cv::Point(playWinSize.width + 43 - smartRocketName.m_size.width, 7 + 5 * (m_nRocketId - 1) + smartRocketName.m_size.height * m_nRocketId);
               m_rocketHitPoint = cv::Point(smartRocketName.m_point.x - 5, smartRocketName.m_point.y - smartRocketName.m_size.height * 0.5);
           }

           void setPlayerNamePoint()
           {
               smartPlayerName.m_point = cv::Point(playWinSize.width + 25 - s_nMaxRocketNameWidth - smartPlayerName.m_size.width, 7 + m_nPlayerNameLabelHeight * 5 + (m_nPlayerNameLabelHeight + 1) * smartPlayerName.m_size.height);
               m_playerHitPoint = cv::Point(nameOwner->smartPlayerName.m_point.x + 5, nameOwner->smartPlayerName.m_point.y - smartPlayerName.m_size.height * 0.5);
           }

           cv::Point playerHitPoint()
           {
               m_playerHitPoint = cv::Point(nameOwner->smartPlayerName.m_point.x - nameOwner->m_nTotalHits * 10 + 5, nameOwner->smartPlayerName.m_point.y - smartPlayerName.m_size.height * 0.5);
               return m_playerHitPoint;
           }

           void putPoint(cv::Mat& img)
           {
               cv::circle(img, m_rocketHitPoint, 2, COLORS[RED], FILLED);        //bod k rocketName
               cv::circle(img, playerHitPoint(), 2, COLORS[RED], FILLED);         //bod k playerName
           }

           void recordHit(double time, cv::Mat& img)
           {
               hit = true;                                         //ze zasah uz prebehol

               m_dHitTime = time;

               nameOwner->m_nLapHits++;
               nameOwner->m_nTotalHits++;

               putPoint(img);
           }

           /*****URCENIE STATISTIK ZASAHU******
           zasah moze nastat, len ak hracova raketa este nedopadla: time <= m_dFlightTime
           zasah je, ked je raketa od stredu terca menej ako 8 pixlov. v tejto polohe moze byt ale po viac sekund =>
           statistiky zasahu zapisujeme len pri 1. kontakte, ked je hit == false*/
           bool freshHit(double time, const Target& target)
           {
               return !hit && time <= m_dFlightTime && target.isHit(timePoint(time + 0.1));
           }

           void checkHit(double time, const Target& target, cv::Mat& img)
           {
               if (freshHit(time, target))
                   recordHit(time, img);
           }

           void lapReset()
           {
               m_nLapHits = 0;                                                               //zasahy iba za TOTO kolo
               m_dHitTime = 0;
               hit = false;
           }


        void mixColor()
        {
            do
            {
                 //namiesa farby pre nasledovnu ciaru, jej text
                 color = cv::Scalar(rand()%255+0, rand()%255+0, rand()%255+0);
            }
            while (tooWhite(190) || tooBlack(30) || hasColor(COLORS[PINK]));
            //nech nie su moc tmave alebo svetle farby. miesaj, pokial je farba zla

            smartPlayerName.m_color = smartRocketName.m_color = color;          //daj farbu stringom
        }

        void printOnClick()
        {
            sprintf(clickString, "\n***%d. hrac: %s***\nSuradnice (%d, %d).\nUhol = %lf st.\nPociatocna rychlost = %lf m/s\n", m_nRocketId, rocketName, linePoint.x, math_y, m_dAngle, m_dSpeed);
            printf("%s", clickString);

            strcpy(allString, clickString);    //v kazdom kole sa pise na zaciatok allString - tu sme mali buffer overflow
        }

        void printParams()      //vytlaci letove parametre a pripoji ich do hracskeho retazca
        {
            sprintf(paramString, "\nD = %lf m\ntd = %lf s\nvd = %lf m/s\nH = %lf m\nth = %lf s\n", m_dFlightLength, m_dFlightTime, m_dFallSpeed, m_dFlightHeight, m_dRiseTime);
            strcat(allString, paramString);
        }


        void printFlight(int targetPos, int targetSize)
        {
            if (!hit)
            {
                if (m_dFlightLength > targetPos + targetSize)
                   sprintf(flightString, "Dopadla %lf m za cielom\n", m_dFlightLength - targetPos + targetSize);
                else if (m_dFlightLength < targetPos - targetSize)
                   sprintf(flightString, "Dopadla %lf m pred cielom\n", targetPos - targetSize - m_dFlightLength);
            }
            else
                sprintf(flightString, "%s zasaiahol ciel\n", rocketName);

            strcat(allString, flightString);
        }


        void printAll(FILE* outFile)
        {
            puts(clickString);
            fputs(clickString, outFile);

            puts(paramString);
            fputs(paramString, outFile);

            puts(flightString);
            fputs(flightString, outFile);
        }

        void printLapResults(bool totals)
        {
            if (!totals)                                                    //vysledky iba tohto kola
                sprintf(resultString, "%s %d/%d zasahov\n", playerName, m_nLapHits, m_nTotalShots);
            else                                                            //celkove vysledky doteraz
                sprintf(resultString, "%s %d/%d zasahov [%.2f%%%%]\n", playerName, m_nTotalHits, m_nTotalShots * thisLap, 100 * (float) m_nTotalHits / (m_nTotalShots * thisLap));
        }


        static vector<Player*> orderResults(vector<Player>& players, const short laps, const bool totals)
        {
            vector<Player*> copyPlayers;
            copyPlayers.reserve(players.size());

            for (auto& p : players)                    //iba si skopirujem polia, aby som robil na kopiach
                if (p.ownsName)
                    copyPlayers.push_back(&p);

            std::sort(copyPlayers.begin(), copyPlayers.end(),
            [totals, laps](Player* p1, Player* p2)
            {
                auto hitsP1 =  !totals ? p1->m_nLapHits : p1->m_nTotalHits;
                auto shotsP1 = !totals ? p1->m_nTotalShots : (p1->m_nTotalShots * laps);

                auto hitsP2 =  !totals ? p2->m_nLapHits : p2->m_nTotalHits;
                auto shotsP2 = !totals ? p2->m_nTotalShots : (p2->m_nTotalShots * laps);

                if (hitsP1 > hitsP2)
                    return true;

                if (hitsP1 == hitsP2 && shotsP1 < shotsP2)
                    return true;

                return false;
            });

        //    printf(totals ? "\ntotals\n" : "\nlap-only\n");
        //    for (int i = 0; i < Player::s_nPlayersCount; i++)
        //        if (!totals)
        //            printf("%d %d %d\t%d %d %d\n", copyPlayers[i]->m_nPlayerId, copyPlayers[i]->lapHits, copyPlayers[i]->ownsName, orderedPlayers[i]->m_nPlayerId, orderedPlayers[i]->lapHits, orderedPlayers[i]->ownsName);
        //        else
        //            printf("%d %d %d\t%d %d %d\n", copyPlayers[i]->m_nPlayerId, copyPlayers[i]->m_nTotalHits, copyPlayers[i]->ownsName, orderedPlayers[i]->m_nPlayerId, orderedPlayers[i]->m_nTotalHits, orderedPlayers[i]->ownsName);

        //    cout << endl;
            return copyPlayers;                                                      //vysledok su prehadzane indexy podla max zasahov
        }


        static void orderResultsAndPrint(vector<Player>& players, short laps, bool totals, FILE* outFile = 0)
        {
            auto orderedPlayers = orderResults(players, laps, totals);

            for (auto orderedPlayer : orderedPlayers)
            {
                orderedPlayer->printLapResults(totals);

                dualPrintf(outFile, orderedPlayer->resultString);
            }
        }

    private:

        bool tooWhite(short treshold)
        {
            return color.val[0] >= treshold && color.val[1] >= treshold && color.val[2] >= treshold;
        }

        bool tooBlack(short treshold)
        {
            return color.val[0] <= treshold && color.val[1] <= treshold && color.val[2] <= treshold;
        }

        bool hasColor(const cv::Scalar& badColor)
        {
            return color == badColor;
        }
    };


    class Game
    {
    public:
        short m_nLaps;

        cv::Size setWin;

        Size2d playWinSize;

        cv::Point rootPoint;

        //Player vecPlayers[];

        SmartString welcome, putTarget, targetPos, targetTooClose, thruTarget, nearTarget, sameAngle, aimShot, setShot, clickNext, animEnd, pressKey, lapLabel;

        Game() = default;

        Game(int h, int l) : m_nLaps(l)
        {
            setWin.height = h;
            setWin.width = h * 2;
        }

        cv::Point centeredAt(int fromWidth, int fromHeight)
        {
            return cv::Point(setWin.width / 2 - fromWidth, setWin.height / 2 - fromHeight);
        }

        void setup(int height, int laps)
        {
            setWin.height = height;
            setWin.width = 2 * setWin.height;                            //rozmery uvitacieho obrazka

            this->m_nLaps = laps;

            rootPoint = cv::Point(0, setWin.height);

            initStrings();                                    //pozname rozmery okna => mozeme don vykreslit stringy
        }

    private:
        void initStrings()
        {
            Point dumbPoint;
            welcome = SmartString(pSetImg, &BIG_FONT, "Vitajte v streleckej simulacii ! ");
            welcome.m_point = Point(setWin.width / 2 - welcome.m_size.width / 2, setWin.height / 2 - welcome.m_size.width / 4);
            dumbPoint = centeredAt(welcome.m_size.width / 2, welcome.m_size.width / 4);

            putTarget = SmartString(pSetImg, &BIG_FONT, "Urci polohu terca ! ");
            putTarget.m_point = Point(setWin.width / 2 - putTarget.m_size.width / 2, setWin.height / 2 - putTarget.m_size.width / 4 + 2*putTarget.m_size.height);
            dumbPoint = centeredAt(putTarget.m_size.width / 2, putTarget.m_size.width / 4 - 2*putTarget.m_size.height);

            targetPos = SmartString(pSetImgCopy, &MEDIUM_FONT, "Poloha terca");     //nema staticky bod

            targetTooClose = SmartString(pSetImgCopy, &BIG_FONT, "Terc prilis blizko ! ");
            targetTooClose.m_point = Point(setWin.width / 2 - targetTooClose.m_size.width / 2, setWin.height / 2-targetTooClose.m_size.height / 2);
            dumbPoint = centeredAt(targetTooClose.m_size.width / 2, targetTooClose.m_size.height / 2);

            aimShot = SmartString(pSetImg, &BIG_FONT, "Namier raketu tak, aby zasiahla ciel ! ");
            aimShot.m_point = Point(setWin.width / 2 - aimShot.m_size.width / 2, setWin.height / 2 - aimShot.m_size.width / 4);
            dumbPoint = centeredAt(aimShot.m_size.width / 2, aimShot.m_size.width / 4);

            setShot = SmartString(pSetImg, &BIG_FONT, "Urci uhol a poc. rychlost.");
            setShot.m_point = Point(setWin.width / 2 - setShot.m_size.width / 2, setWin.height / 2 - setShot.m_size.width / 4 + 2 * setShot.m_size.height);
            dumbPoint = centeredAt(setShot.m_size.width / 2, setShot.m_size.width / 4 - 2*setShot.m_size.height);

            thruTarget = SmartString(pSetImgCopy, &BIG_FONT, "Raketa prechadza tercom ! ");
            thruTarget.m_point = Point(setWin.width / 2 - thruTarget.m_size.width / 2, setShot.m_point.y + 1.5 * thruTarget.m_size.height);
            dumbPoint = centeredAt(thruTarget.m_size.width / 2, 3 * thruTarget.m_size.height);

            nearTarget = SmartString(pSetImgCopy, &BIG_FONT, "Velmi blizko terca ! ");
            nearTarget.m_point = Point(setWin.width / 2 - nearTarget.m_size.width / 2, setShot.m_point.y + 1.5 * thruTarget.m_size.height);
            dumbPoint = centeredAt(nearTarget.m_size.width / 2, 3 * nearTarget.m_size.height);

            sameAngle = SmartString(pSetImgCopy, &BIG_FONT, "Totozny uhol! Zvol iny uhol.");
            sameAngle.m_point = Point(setWin.width / 2 - sameAngle.m_size.width / 2, setWin.height / 2 - sameAngle.m_size.height / 2);
            dumbPoint = centeredAt(sameAngle.m_size.width / 2, sameAngle.m_size.height / 2);

            clickNext = SmartString(pSetImg, &BIG_FONT, "Klikni pre pokracovanie");
            clickNext.m_point = Point(setWin.width / 2 - clickNext.m_size.width / 2, setWin.height / 2 - clickNext.m_size.width / 4);
            dumbPoint = centeredAt(clickNext.m_size.width / 2, clickNext.m_size.width / 4);

            animEnd = SmartString(pPlayImg, &MEDIUM_FONT, "Koniec animacie.");
            animEnd.m_point = Point(60, 7 + animEnd.m_size.height);

            pressKey = SmartString(pPlayImg, &MEDIUM_FONT, "Stlac klavesu.");
            pressKey.m_point = Point(60, 2 * (7 + pressKey.m_size.height));

            lapLabel = SmartString(pSetImg, &BIG_FONT);
        }
    };

    Game game;                      //global game object

	vector<Player> vecPlayers;

    Target Terc;                    //global target object


    //*******inicializacia statickych prem******
    //triedy Player
    int Player::s_nRocketId = 0;
    int& Player::s_nWinHeight = game.setWin.height;
    int Player::s_nPlayerId = 0;
    int Player::s_nMaxRocketNameWidth = 0;
    int Player::s_nMaxHitCountWidth = 0;
    int Player::s_nPlayersCount = 0;

    Size2d& Player::playWinSize = game.playWinSize;


    int smartScanf(FILE* stream, const char* format, void* pointer)
    {
        int ret = fscanf(stream, format, pointer);                              //skus zo suboru

        if (!ret)                                                               //ak nejde
            ret = scanf(format, pointer);                                         //skus z konzoly

        return ret;
    }


    //#include <getopt.h>

    int run(int argc, char *argv[])                                    //Hlavny program
    {
    //    static struct option long_options[] =
    //    {
    //        /* These options set a flag. */
    ////        {"verbose", no_argument,       &verbose_flag, 1},
    ////        {"brief",   no_argument,       &verbose_flag, 0},
    //        /* These options don't set a flag.
    //          We distinguish them by their indices. */
    //        {"debug",   no_argument,       0, 'd'},
    //        {"input",   required_argument, 0, 'i'},
    //        {"output",  required_argument, 0, 'o'},
    //        {0}
    //    };

    //    int option, option_index = 0;
    //    bool debugInfo = false;

    //    while ((option = getopt_long(argc, argv, "di:o:", long_options, &option_index)) != -1)
    //    {
    //        switch (option)
    //        {
    //        case 'd':
    //            debugInfo = true;
    //            break;

    //        case 'i':
    //            inputFile = fopen(optarg, "r");
    //            break;

    //        case 'o':
    //            outFile = fopen(optarg, "w");
    //            break;

    //        case '?':
    //            break;              /* getopt_long already printed an error message. */
    //        }
    //    }


        if (!inputFile)                //mena citame zo suboru. ak taky subor neni, citame zo stdin
            inputFile = stdin;

        if (!outFile)
            outFile = fopen("vysledky.txt", "w");                             //vysledky do suboru


        int bas = 0;
        cv::Size hitLabelSize = cv::getTextSize("ZASAH ! ", MEDIUM_FONT.face, MEDIUM_FONT.scale, MEDIUM_FONT.thickness, &bas);                                  //pozostatok, ale pomaha


        /*scanf consumes only the input that matches the format string, returning the number
         of characters consumed. Any character that doesn't match the format string causes it to stop
         scanning and leaves the invalid character still in the buffer*/
        int setHeight;
        do
        {
          printf("Zadaj vysku obrazka <300, 450 > : ");

          smartScanf(inputFile, "%d", &setHeight);

          flushAll(inputFile);
        }
        while (setHeight < 300 || setHeight > 450);                         //pri vacsich rozmeroch nemusi byt schopny alokovat pamat

        do
        {
           printf("\nZadaj pocet hracov (max. 10): ");

           smartScanf(inputFile, "%d", &Player::s_nPlayersCount);

           flushAll(inputFile);                                   //keby zadal zle
        }
        while (Player::s_nPlayersCount < 1 || Player::s_nPlayersCount > 10);

        int laps;
        do
        {
           printf("\nZadaj pocet kol (max. 10): ");

           smartScanf(inputFile, "%d", &laps);

           flushLine();
        }
        while (laps < 1 || laps > 10);


        game.setup(setHeight, laps);


        puts("\nZadaj mena hracov. Ak sa bude meno opakovat, hrac dostane dalsiu raketu.");

        if (outFile)
            fprintf(outFile, "Pocet hracov: %d", Player::s_nPlayersCount);


		vecPlayers.resize(Player::s_nPlayersCount);


        for (auto& player : vecPlayers)                       //nacita mena
        {
            do
            {
                printf("\n%d.hrac: ", player.m_nPlayerId);

                fscanf(inputFile, "%10s", player.playerName);           //meno zo suboru, max 10 znakov

                //ak ma nacitane meno 10 znakov a na riadku su este znaky, citaj ich kym neprides na koniec
                //ak nema, scanf prejde na dalsi riadok sam
                if (strlen(player.playerName) >= 10)
                    flushLine(inputFile);


                //ak v mene najdeme iny znak ako medzera => emptyName = false a ideme na dalsie meno. inak je cele meno plne medzier
            }
            while (isWhiteSpaceString(player.playerName));
        }


        for (short i = 0, j, playerNamePos_Y = 0; i < Player::s_nPlayersCount; i++)
        {
            //meno tohto hraca porovnava s hracmi pred nim. hrac narazi na rovnake meno:
            //sameCount sa zvisi TOMUTO hracovi.
            //najdeny hrac je nameOwner TOHTO hraca.
            //cyklus ide na dol => na konci bude sameCount TOHTO hraca rovny poctu hracov PRED NIM, kt maju rovnake meno
            //nameOwner bude PRVY majitel mena


            for (j = i - 1; j >= 0; j--)
                if (strcmp(vecPlayers[i].playerName, vecPlayers[j].playerName) == 0)
                {
                   vecPlayers[i].ownsName = false;

                   vecPlayers[i].nameOwner = &vecPlayers[j];                        //prvy vyskyt mena, co sa opakuje
                }

            strcpy(vecPlayers[i].rocketName, vecPlayers[i].playerName);             //rocketName sa lisi len v (pocet_raket)


            //sameCount > 0 vravi, ze hrac neni majitel mena. jeho totalShots = 0 a majitelove totalShots sa zvysia

            //majitelia  :  sameCount == 0,  totalShots > 0
            //NEmajitelia:  sameCount > 0,  totalShots == 0

            if (!vecPlayers[i].ownsName)
            {
               vecPlayers[i].m_nTotalShots= 0;
               vecPlayers[i].nameOwner->m_nTotalShots++;             //vyhadruje kolky je tento hrac co ma toto meno

               sprintf(vecPlayers[i].rocketName, "%s(%d)", vecPlayers[i].rocketName, vecPlayers[i].nameOwner->m_nTotalShots);
            }
            else
                vecPlayers[i].m_nPlayerNameLabelHeight  = playerNamePos_Y++;
                //hrac[i].m_nPlayerNameLabelHeight = i; //playerName bude vedla majitelovho rocketName

            //musia byt vytvorene pred miesanim farieb!
            vecPlayers[i].smartPlayerName.setText(vecPlayers[i].playerName);
            vecPlayers[i].smartRocketName.setText(vecPlayers[i].rocketName);

            vecPlayers[i].mixColor();                                 //farby priradi EXISTUJUCIM smartStringom
        }


        for (size_t i = 0; i < vecPlayers.size(); i++)                  //treba znamu velkost vsetkych smartRocketName
            Player::s_nMaxRocketNameWidth = max(vecPlayers[i].smartRocketName.m_size.width, Player::s_nMaxRocketNameWidth);


        char kola[15];
        double gameTime = -1;

        //******ZADAVANIE RAKIET*******
        setImg.create(game.setWin.height, game.setWin.width, CV_8UC3);
        setImg.copyTo(setImgCopy);                           //KLONUJEME AZ TU, ABY SA UKAZALI VECI CO SME NEDAVNO NAKRESLILI
        cv::namedWindow("Graf");                             //okno pre obrazky

        //mouseHandler bude callback pre mouse eventy. Graf uz MUSI BYT VYTVORENY
        cv::setMouseCallback("Graf", mouseHandler);


        for (thisLap = 1; thisLap <= game.m_nLaps; thisLap++)                             //ITERACIA KOL
        {
            if (game.m_nLaps > 1)                                 //label kola sa pise len ak hra trva viac kol
            {
               sprintf(kola, "***%d.KOLO***", thisLap);

               game.lapLabel.setText(kola);
               game.lapLabel.m_point = cv::Point(game.setWin.width / 2 - game.lapLabel.m_size.width / 2, 20);
               game.lapLabel.display(COLORS[PINK]);

               dualPrintf(outFile, "\n\n%s\n", kola);
            }

            game.welcome.display(COLORS[PINK]);
            game.putTarget.display(COLORS[PINK]);

            setImg.copyTo(setImgCopy);

            cv::setMouseCallback("Graf", mouseHandler);   //ked sa okno znici, zanikne aj jeho vezba na mouseHandler

            do
            {
                //tento kod dojde sem, ukaze obrazok a CAKA na stlacenie key, kedy nakresli obrazok znova. inak nerobi nic
                imshow("Graf", setImgCopy);
                cv::waitKey();                                   //TU SA ZASTAVI SLUCKA! po stlaceni key sa zacne animacia
            }
            while (clickControl < (int)vecPlayers.size());    //clickControl sa zvysuje v mouseHandler. dalej sa ide az po zadani vsetkych rakiet


            //tu uz nepotrebujeme okna zadavania rakiet
            setImg.setTo(0);
            cv::destroyWindow("Graf");                        //TOTO TU MUSI BYT, ABY MOUSEHANDLER NEPRACOVAL V PLAYIMG
            //******ZADAVANIE RAKIET KONIEC*******


            clickControl = TARGET_CLICK;
			game.playWinSize = {0, 0};                                                                  //vsetky 3 sa pre dalsie kolo vynuluju

			for (auto& player : vecPlayers)
            //for (int i = 0; i < Player::s_nPlayersCount; i++)                                                           //velicinovy cyklus
            {
				player.printParams();

                game.playWinSize.width = max(player.m_dFlightLength, game.playWinSize.width);                                         //najdlhsia dlzka doletu je kandidatom pre dlzku obrazka

                game.playWinSize.height = max(player.m_dFlightHeight, game.playWinSize.height);                                          //najdlhsia vyska doletu


                /******URCENIE DOBY HRY******
                doba animacie hraca: animTime

                rozmery okna:
                    dlzka max 1000, vyska max 600

                ak hracova raketa preleti dlzku okna:
                    ak nepreleti aj vysku okna, bere sa len cas, za kt preleti 1000m.
                    ak preleti aj vysku okna, bere sa len cas, za kt skor preleti vysku / dlzku okna.

                ak nepreleti dlzku okna: m_dFlightTime = cas dopadu (aj ked vyleti hodne vysoko)

                hrac s najdlhsim dopadom = gameTime. ak ma hrac dlhsi dopad ako je sucasny gameTime,
                gameTime = tento dopad. */

                double animTime;

                if (player.m_dFlightLength > 1000)
                {
                    if (player.m_dFlightHeight <= 600)
                        animTime = 1000 / player.v0x;
                    else
                        animTime = min(1000/player.v0x, 600/player.v0y);
                }
                else
                    animTime = player.m_dFlightTime;

                if (animTime > gameTime)
                    gameTime = animTime;


                player.lapReset();              //reset statistik pre toto nove kolo

                //max kombinacia dlzka mena + pocetnost*kolo
                Player::s_nMaxHitCountWidth = max((player.m_nTotalHits + player.m_nTotalShots) * 10 + player.smartPlayerName.m_size.width, Player::s_nMaxHitCountWidth);
            }


            game.playWinSize.width = max(game.playWinSize.width, tarPos + hitLabelSize.width / 2.0 + 4);                             //aby sa zmestil terc

            game.playWinSize.width = max(game.playWinSize.width, 37.0 + Player::s_nMaxRocketNameWidth + Player::s_nMaxHitCountWidth + game.animEnd.m_size.width);             //aby sa zmestila legenda pri malom obraze


            //ak nejaka raketa ma dolet viac ako 1000m, zobrazi sa len po 1000
            if (game.playWinSize.width > winWidthMax)
                game.playWinSize.width = winWidthMax;
            else if (game.playWinSize.width < winWidthMin)
                game.playWinSize.width = winWidthMin;


            //ak je potrebna vyska MENSIA jak legenda, da sa tak, aby sa vosla legenda
            game.playWinSize.height = max(game.playWinSize.height, 28 + hitLabelSize.height + 7 + Player::s_nPlayersCount * vecPlayers[0].smartPlayerName.m_size.height + (Player::s_nPlayersCount - 1) * 5.0);

            if (game.playWinSize.height > winHeightMax)
                game.playWinSize.height = winHeightMax;
            else if (game.playWinSize.height < winHeightMin)
                game.playWinSize.height = winHeightMin;


            //TU UZ SU ROZMERY ROCKET OKNA JASNE
            playImg.create(game.playWinSize.height + 30, game.playWinSize.width+X_OFFSET, CV_8UC3);                                    //obrazok animacie
            playImg.setTo(0);

            cv::line(playImg, cv::Point(X_OFFSET, game.playWinSize.height), cv::Point(X_OFFSET + game.playWinSize.width, game.playWinSize.height), COLORS[WHITE]);                //os X
            cv::line(playImg, cv::Point(X_OFFSET, 0), cv::Point(X_OFFSET, game.playWinSize.height), COLORS[WHITE]);                                  //os Y

            Terc.setScreenPoint(game.playWinSize.height);
            //Player::playWinSize = game.playWinSize;


            //***********OSI A ZNACKY***************
            short labelStep = 50, thisLabel;

            SmartString smallLabel(pPlayImg, &SMALL_FONT, COLORS[YELLOW]),
                        bigLabel(pPlayImg, &MEDIUM_FONT, COLORS[LIGHTBLUE]);

			for (short m = 0; (thisLabel = labelStep * m) < max(game.playWinSize.width, game.playWinSize.height); m++)                                          //na osach odkrokuje markery po 50 m
			{
				smallLabel.setNumber(thisLabel);
				bigLabel.setNumber(thisLabel);


				Line X_line(cv::Point(X_OFFSET + thisLabel, game.playWinSize.height - 2), cv::Point(X_OFFSET + thisLabel, game.playWinSize.height + 2));        //X_POS je rovnaka, meni sa vyska
				Line Y_line(cv::Point(X_OFFSET - 2, game.playWinSize.height - thisLabel), cv::Point(X_OFFSET + 2, game.playWinSize.height - thisLabel));        //Y_POS je rovnaka, meni sa dlzka


				//*****VODOROVNE OSI******
				//50 * m je aktualna znacka
				//50 * m % 100 == 0: presne 100vky na osi
				//50 * m % 100 != 0: presne 50vky na osi. NEMOZNO % 50, lebo tomu vyhovuju aj 100vky

				//abs(50 * m - T) >= 30: je od terca vzdialene viac ako 30m. ak je blizsie, nezobrazi sa
				//50*m+tex.width / 2 < dlz: pravy kraj labelu osi nevybehne z okna

				if (abs(thisLabel - tarPos) >= 30)
				{
					if (m % 2 == 0)                                                           //bigLabel
					{
						if (thisLabel + bigLabel.m_size.width / 2 <= game.playWinSize.width)
						{
							cv::line(playImg, X_line.A, X_line.B, COLORS[LIGHTBLUE]);
							bigLabel.display(cv::Point(X_OFFSET + thisLabel - bigLabel.m_size.width / 2, game.playWinSize.height + 15 + bigLabel.m_size.height / 2));
						}
					}
					else                                                                      //smallLabel
						if (thisLabel + smallLabel.m_size.width / 2 <= game.playWinSize.width)
						{
							cv::line(playImg, X_line.A, X_line.B, COLORS[YELLOW]);
							smallLabel.display(cv::Point(X_OFFSET + thisLabel - smallLabel.m_size.width / 2, game.playWinSize.height + 15 + smallLabel.m_size.height / 2));
						}
				}

				//*******ZVISLE OSI********
				//m > 0: v bode [0, 0] sa nekresli zvisla os, ale iba vodorovna
				if (m > 0)
				{
					if (m % 2 == 0)
					{
						if (game.playWinSize.height - bigLabel.m_size.height / 2 - thisLabel > 0)              //100VKY
						{
							cv::line(playImg, Y_line.A, Y_line.B, COLORS[LIGHTBLUE]);
							bigLabel.display(cv::Point(25 - bigLabel.m_size.width / 2, game.playWinSize.height + bigLabel.m_size.height / 2 - thisLabel));
						}
					}
					else if (game.playWinSize.height - smallLabel.m_size.height / 2 - thisLabel > 0)              //50tky
					{
						cv::line(playImg, Y_line.A, Y_line.B, COLORS[YELLOW]);
						smallLabel.display(cv::Point(25 - smallLabel.m_size.width / 2, game.playWinSize.height + smallLabel.m_size.height / 2 - thisLabel));
					}
				}
			}                                                                        //koniec osoveho cyklu

    //        Terc = Target(img1, T, vys, 8, PINK, &m_font);

            for (auto& player : vecPlayers)                                        //LABLE A BODY HRACOV
            {
                player.setRocketNamePoint();
                player.smartRocketName.display();

                if (player.ownsName)                       //playerName sa zobrazi len majitelovi
                {
                    player.setPlayerNamePoint();

                    player.smartPlayerName.display();
                }


                for (int hit = 1; hit <= player.m_nTotalHits; hit++)                                  //body z inych kol
                    cv::circle(playImg, cv::Point(player.smartPlayerName.m_point.x + 5 - hit * 10, player.nameOwner->smartPlayerName.m_point.y - player.smartPlayerName.m_size.height / 2), 2, COLORS[RED], FILLED);
            }


            bool animationEnded, redTarget;

            for (double curTime = 0; curTime <= gameTime; curTime += 0.1)        //iteracia == snimka
            {
                redTarget = false;                                      //v kazdej snimke sa zisti, aky treba terc
                animationEnded = true;

				for (auto& player : vecPlayers)
                {                                                       //momentalna draha
                   if (!player.hit && curTime <= player.m_dFlightTime)
                   {
                       cv::line(playImg, player.flightPoint(curTime), player.flightPoint(curTime + 0.1), player.color);
                       animationEnded = false;
                   }

    //               for (double pastTime= 0; pastTime <= curTime; pastTime += 0.1)      //predosla draha
    //                   /* ! hrac[i].hit && pastTime <= hrac[i].m_dFlightTime:
    //                    kresli nevybuchnutu drahu. bez dodatocnej podmienky !hrac[i].hit by po vybuchu kreslila cez terc

    //                    hrac[i].hit && pastTime <= hrac[i].m_dHitTime:
    //                    ak uz vybuchla, kresli len po dotyk s tercom*/
    //                   if (!hrac[i].hit && pastTime <= hrac[i].m_dFlightTime || hrac[i].hit && pastTime <= hrac[i].m_dHitTime)
    //                      cv::line(img1, hrac[i].flightPoint(pastTime), hrac[i].flightPoint(pastTime + 0.1), hrac[i].m_color);
                }

                if (animationEnded)                 //ziadna raketa sa neanimovala
                    break;                          //konci celu animaciu


				for (auto& player : vecPlayers)                                        //LABLE A BODY HRACOV
                {
                    player.checkHit(curTime, Terc, playImg);

                    if (!redTarget && player.hit && curTime <= player.m_dHitTime + 0.2)          //po zrazke zobrazi vybuch pocas 0, 2s
                        redTarget = true;
                }                                      //koniec cykla zrazok a bodovania


                //ak sa ma nakreslit terc istej farby a uz je tam, NEKRESLI SA
                if (redTarget)                   //terc ma byt cerveny
                    Terc.draw(COLORS[RED]);
                else                             //terc ma byt ruzovy a neni => nakresli ho
                    Terc.draw();


                imshow("Graf", playImg);                                          //vsetko to ukaze
                cv::waitKey(70);                                                      //toto casuje framy. def = 70
            }                                                                       //koniec cyklu animacie


            game.animEnd.display(COLORS[PINK]);
            game.pressKey.display(COLORS[PINK]);

            imshow("Graf", playImg);                               //TREBA UKAZAT KONIEC
        	playImg.release();


            //HRACIE OKNO TU ESTE NENI ZNICENE ! !!
			for (auto& player : vecPlayers)
            {
                player.printFlight(tarPos, Terc.m_size);

                dualPrintf(outFile, player.allString);
                //hrac[i].printAll(outFile);
            }


            if (thisLap > 1)                                      //po 1. kole vypis len celkove vysledky - toto nepis
            {
                dualPrintf(outFile, "\n\n***V %d. KOLE***\n", thisLap);

                Player::orderResultsAndPrint(vecPlayers, thisLap, false, outFile);             //zoradi vysledky kola
            }


            if (thisLap < game.m_nLaps && game.m_nLaps > 1)                                       //medzi prvym a poslednym kolom
                dualPrintf(outFile, "\n\n***PO %d. KOLE***\n", thisLap);
            else if (thisLap == game.m_nLaps)                                                 //posledne kolo
                dualPrintf(outFile, "\n\n***VYSLEDKY***\n");

            Player::orderResultsAndPrint(vecPlayers, thisLap, true, outFile);


            cv::waitKey(0);                                       //caka na skoncenie hracieho okna
        }                                                       //koniec cyklu kola


        //CLEANUP
        fclose(outFile);
        fclose(inputFile);

        setImg.release();
        setImgCopy.release();
        playImg.release();

        cv::destroyWindow("Graf");

        puts("");

#ifdef _WIN32
        system("pause");
#endif

        return EXIT_SUCCESS;
    }


    bool sameAngle, thruTarget, nearTarget;

    void drawSetShot(Player& player, int x, int y, cv::Mat& img)
    {
        player.smartRocketName.display(cv::Point(x+5, y-5), &img);
        cv::line(img, game.rootPoint, cv::Point(x, y), player.color);              //aktualna pohybliva ciara
    }


    void mouseHandler(int event, int x, int y, int flags, void *param)
    {
        int invert_y = game.setWin.height - y;

        //srand(time(NULL));
        switch(event)
        {
            case EVENT_LBUTTONDOWN:
            {
                 if (clickControl == TARGET_CLICK)
                 {
                    tarPos = x;                                                //suradnica terca sa ulozi

                    if (tarPos < 100)
                        printf("\nTerc prilis blizko ! \n");
                    else                                                //ak neni terc moc blizko, ide sa dalej
                    {
                        Terc = Target(pPlayImg, tarPos, 8, COLORS[PINK], &MEDIUM_FONT);      //vytvori terc

                        clickControl = 0;                                          //ide sa dalej

                        dualPrintf(outFile, "\nPoloha terca: %d m\n", tarPos);
                    }
                 }
                 else if (TARGET_CLICK < clickControl && clickControl < Player::s_nPlayersCount)                           //kliky rakiet
                 {
                    vecPlayers[clickControl].calcParams(cv::Point(x, y));           //na tomto sa robia nasledovne checky


                    sameAngle = false;

                    if (clickControl > 0)                                                        //robi sa od druheho hraca vyssie
                       for (int j = clickControl - 1; j >= 0; j--)
                           if (abs(vecPlayers[clickControl].m_dAngle - vecPlayers[j].m_dAngle) <= 2)                             //nemoze byt skoro totozny uhol
                           {
                              sameAngle = true;
                              //printf("\nTotozny uhol! Zvol iny uhol.\n");                       //vypise toto iba raz
                              break;
                           }


                    if (!sameAngle && !nearTarget && !thruTarget)         //klik presiel vsetkymi 3 kriteriami
                    {
                        vecPlayers[clickControl].printOnClick();

                        clickControl++;                                 //len tu sa ide na dalsieho hraca
                    }
                 }
                 else
                     clickControl++;                                    //okno zmizne


                 //klikanie rakiet. NAKRESLI NEPOHYBLIVE CASTI pre dalsieho hraca
                 //3 kategorie: 1) co sa NEMENI NIKDY; CO SA MENI s kazdym hracom; co sa meni s obrazkom

                 if (TARGET_CLICK < clickControl && clickControl <= Player::s_nPlayersCount)
                 {
                     setImg.setTo(0);                  //setImg sa pre noveho hraca nakresli odznova
                     //cvSet(img, cv::Scalar(0, 0, 0));

                     cv::circle(setImg, cv::Point(tarPos, game.setWin.height), 8, COLORS[PINK], FILLED);  //nakresli terc znova

                     for (short idx = 0; idx < clickControl; idx++)                             //vsetky ciary doteraz
                         cv::line(setImg, game.rootPoint, vecPlayers[idx].linePoint, vecPlayers[idx].color);


                     if (clickControl == Player::s_nPlayersCount)                                          //vsetky rakety zadane
                     {
                         if (game.m_nLaps > 1)
                            game.lapLabel.display(COLORS[PINK]);

                         game.clickNext.display(COLORS[PINK]);
                     }
                     else                            //TARGET_CLICK < clickControl && clickControl < Player::s_nPlayersCount
                     {
                         if (game.m_nLaps > 1)
                            game.lapLabel.display(vecPlayers[clickControl].color);

                         game.aimShot.display(vecPlayers[clickControl].color);
                         game.setShot.display(vecPlayers[clickControl].color);


                        //len pre to, aby ukazalo vo farbach dalsieho hraca hned po kliku a nemuselo cakat na prvy mousemove
                         //dalsie funkcie kreslia do setImgCopy => musime ho skopirovat
                        setImg.copyTo(setImgCopy);

                         {
                             if (thruTarget)
                                 game.thruTarget.display(vecPlayers[clickControl].color);

                             if (nearTarget)
                                 game.nearTarget.display(vecPlayers[clickControl].color);

                             if (sameAngle)
                                 game.sameAngle.display(vecPlayers[clickControl].color);

                             drawSetShot(vecPlayers[clickControl], x, y, setImgCopy);
                         }

                         imshow("Graf", setImgCopy);
                     }
                 }
            }
            break;


        case EVENT_MOUSEMOVE:
            //setImg obsahuje NEPOHYBLIVE casti. po kazdom kliku sa prekresli v OnClick => pise don len OnClick!
            //OnMove nebude zakazdym nanovo kreslit cely setImg. kresli len pohyblive casti do kopie setImg = setImgCopy
            //aby v setImgCopy neostalo co sa nakreslilo v predoslom OnMove evente => setImg skopiruje sa nanovo

            //cvCloneImage: Makes a full copy of an image, including the header, data, and ROI. The returned cv::Mat& points to the image copy.


            //clickControl <= Player::s_nPlayersCount:
            //cast clickControl == Player::s_nPlayersCount => aby sa setImgCopy neklonoval pri kazdom OnMove, ale IBA RAZ po poslednom kliku


            if (TARGET_CLICK <= clickControl && clickControl <= Player::s_nPlayersCount)
            {
                setImg.copyTo(setImgCopy);

                if (clickControl == TARGET_CLICK)               //KLIKANIE TERCA
                {
                    int labelPoint_X = x - game.targetPos.m_size.width / 2;             //defaultna poloha terca

                    if (x + game.targetPos.m_size.width / 2 > game.setWin.width)              //napis vybieha napravo
                        labelPoint_X = game.setWin.width -1 - game.targetPos.m_size.width;
                    else if (x - game.targetPos.m_size.width / 2 < 0)                  //napis vybieha nalavo
                        labelPoint_X = 0;


                    game.targetPos.display(cv::Point(labelPoint_X, game.setWin.height - 15), COLORS[PINK]);

                    cv::circle(setImgCopy, cv::Point(x, game.setWin.height), 8, COLORS[PINK], FILLED);                                //pohyblivy terc

                    if (x < 100)                                                          //varovanie
                        game.targetTooClose.display(COLORS[PINK]);
                }

                //je v (TARGET_CLICK < clickControl && clickControl < Player::s_nPlayersCount)
                else if (clickControl < Player::s_nPlayersCount)                       //KLIKANIE RAKIET
                {
                    //x > tarPos - 8 : X suradnica rakety moze byt vnutri terca
                    //invert_y / x == sklon priamky
                    //tarPos * sklon <= Terc.m_size : ciara v mieste x = tarPos prechadza tercom
                    thruTarget = x > tarPos - Terc.m_size && tarPos * invert_y / x <= Terc.m_size;       //raketa prechadza tercom


                    // !thruTarget: thruTarget == true => nearTarget je automaticky false. inak moze byt hocico
                    //x < T : X suradnica rakety je nalavo od terca. keby sme boli hore, raketa by uz preletela
                    //dist > 8 : dotyka sa akurat okraja terca
                    nearTarget = !thruTarget && x < tarPos && numberIsBetween(squareDist(tarPos-x, invert_y), Terc.m_size, 25);

                    {
                        if (thruTarget)
                            game.thruTarget.display(vecPlayers[clickControl].color);

                        if (nearTarget)
                            game.nearTarget.display(vecPlayers[clickControl].color);

                        if (sameAngle)
                            game.sameAngle.display(vecPlayers[clickControl].color);


                        drawSetShot(vecPlayers[clickControl], x, y, setImgCopy);
                    }
                }

                else                            //je v clickControl == Player::s_nPlayersCount
                    //pokial hrac neklikne => clickControl ostane rovne Player::s_nPlayersCount => if hore prejde
                    //a bude sa prekreslovat nepohyblivy obrazok => zvysime clickControl, aby if nepresiel
                    clickControl++;


                imshow("Graf", setImgCopy);
            }
            break;
        }
    }


    double sinalfa(double v0, double alfa)
    {
        return v0 * sin(degToRad(alfa));
    }

    double cosalfa(double v0, double alfa)
    {
        return v0 * cos(degToRad(alfa));
    }

    double fallTime(double v0y)
    {
        return 2 * v0y / g;
    }

    double Y_timePoint(double v0y, double t)
    {
        return v0y * t - g * 0.5 * pow(t, 2);
    }

    double X_timePoint(double v0x, double t)
    {
        return v0x * t;
    }

    double Y_speed(double v0y, double t)
    {
        return v0y - g * t;
    }

    double X_speed(double v0x)
    {
        return v0x;
    }

    double squareDist(double x, double y)                  //pytagorova veta
    {
        return sqrt(pow(x, 2) + pow(y, 2));
    }

    double uhol(double y, double x)
    {
        return radToDeg(atan(y / x));
    }

    template < typename T> void printArray(T *z, int p)
    {
        for (int i = 0; i < p; i++)
            cout << z[i] << endl;

        puts("");
    }
}


int main(int argc, char* argv[])
{
	return rockets::run(argc, argv);
}
