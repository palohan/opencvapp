#pragma once

#include <opencv2/opencv.hpp>
#include "utils.h"

enum class Plane : char {horizintal, vertical, none};

enum Side
{
	up = 0x1,
	down = 0x2,
	left = 0x4,
	right = 0x8,
	none = 0,
	all = 15
};

struct RelativePosition
{
	Side side = Side::none;
    int distance = 0;
};

class Square
{
public:
    class Corner : public MyCvPoint
    {
    public:
        Corner() = default;

        Corner(int x, int y);

        Corner(const MyCvPoint& p);

        Direction directionToNeighbor(const Corner& other) const;

        Plane getSamePlane(const Corner& other) const;

        cv::Point getBorderPoint(bool isLeft, size_t delta = 0) const;

    private:
        void adjust();
    };


private:
    static cv::Mat* m_pImg;

	static size_t s_nTotalSize, s_nSize, s_nMargin;
	static int s_nSmer;

public:
    int m_iterColor;

    Corner m_cornerLeft, m_cornerRight;
    cv::Scalar m_color;
	
    Square();

    Square(const Corner &corner);

    bool operator ==(const Square& other) const;

    bool overlapsOther(const Square& other) const;

    void draw(bool selected = false) const;

public:
    static void setImage(cv::Mat& image);
	
	static size_t getSize();
	static size_t getMargin();
	static size_t getTotalSize();

	static void setDimensions(size_t size, size_t padding);

    Square movedSquare(char key) const;

    void giveColor(ColorManager& manager);

    void adjustPoints(const Corner &corner);

    int getColorIterator() const;

    void move(char direction);

    Plane getSamePlane(const Square& other) const;

    RelativePosition relativePositionToOther(const Square& other) const;
    bool isInVicinityOfOther(const Square& other, int vicinity, int &distance) const;

    bool containsPoint(const Corner& corner) const;
	    
	void highlight(const cv::Scalar& color, size_t thickness = 1) const;

private:
    //void handleDirection(char key) const;
    MyCvPoint center() const;
};


