#include <cstdlib>
#include <cstdio>
#include "snake.h"
#include "feeder.h"

constexpr auto stopped = 200;


Snake::Snake(const Square& s, Feeder& _feeder, bool overlaps) :
    m_canOverlap(overlaps),
    m_prevPosition(s.m_cornerLeft)
{
    addSquare(s);

    {
        m_feeder = &_feeder;
//        m_feeder->addSnake(*this);

//        printf("snake at %p has m_feeder at %p\n", this, this->m_feeder);
    }
}

//Snake::Snake(const Snake& other)
//{
//    printf("Copies snake at %p to new snake at %p", &other, this);
//}

Snake::~Snake()
{
    puts("Killing snake");
}


void Snake::addSquare(const Square& s)
{
    Square sq(s);

    sq.giveColor(m_colorManager);
    m_squares.push_back(sq);                  //posledny stvorec ma id = chain.m_size - 1
    //chain.push_front(sq);
}


Square::Corner Snake::getPosition() const
{
    return m_squares.back().m_cornerLeft;
}


Action Snake::handleGrowing(char key)
{
    Action action = Action::none;

//    if (key == control::highlight)
//        snake.highlight();
//    else if (key == control::clone)
//        ;
//    else if (key == control::newSnake)

    switch(key)
    {
    case control::select:
        if (!m_isSelected)
            action = Action::select;
        break;

    case control::clone:
        if (m_prevAction == Action::select)
            action = Action::clone;
        else if (m_isSelected)
        {
            action = Action::cloneKill;
            m_isSelected = false;
        }
        break;

    case control::cloneDone:
        if (m_isSelected && !m_prevPosition.overlaps(getPosition()))
        {
            ;
        }
        break;

    case control::left:
    case control::right:
    case control::up:
    case control::down:
        if (m_prevAction != Action::select)
            action = Action::grow;
        break;

    case control::moveLeft:
    case control::moveRight:
    case control::moveUp:
    case control::moveDown:
        action = Action::shift;
        break;

    case control::pop:
        action = Action::pop;
        break;

    default:;
    }

    return action;
}


Action Snake::handleSpeed(char key)
{
    /*space - koniec hry
     t:
        ismoving == true -> stop
        else -> resume

     w a s d:
        ismoving = true -> zmen smer
        else -> posun o 1, zmen smer, ismoving ostava false

    ine:
        m_bMoving -> nic
        else -> posun o 1 v smere, ismoving ostava false
     */

    if (m_bFirstStart)
    {
        m_bMoving = true;
        m_bFirstStart = false;
    }

    bool advance = false;
    Action action = Action::none;


    switch(key)
    {
    case control::slower:
        if (m_speed < stopped)
            m_speed += 10;
        break;                                        //pridanie spomaluje

    case control::faster:
        if (m_speed > 10)
            m_speed -= 10;                            		//odobranie zrychluje
        break;

    case control::pause:
        if (m_bMoving)
        {
            m_speedPrevious = m_speed;
            m_speed = stopped;
        }
        else
            m_speed = m_speedPrevious;

        m_bMoving = !m_bMoving;
        break;


    case control::moveLeft:
    case control::moveRight:
    case control::moveUp:
    case control::moveDown:
        if (!m_bMoving)
            action = Action::shift;
        break;

    case control::pop:
        action = Action::pop;
        break;

    default:
        advance = true;
    }

    //m_bMoving = moving;

    if (action == Action::none)
        action = advance || (m_bMoving && getSpeed() > 0) ? Action::move : Action::stay;       //vrati ci sa pohne

    return action;
}


PictureAction Snake::handleMovement(char key)
{
    PictureAction picAction = PictureAction::redraw;

    Action action = handleSpeed(key);

    switch (action)
    {
    //MOVE SEKCIA
    case Action::stay:
        picAction = PictureAction::keep;
        break;

    case Action::move:
        if (!move(key))
            return PictureAction::finish;                               //vraca bool;narazil do seba=false

        if (m_feeder->isFeedingTime())
            m_feeder->placeFood();
        break;

    case Action::shift:
        if (!shift(key))
            picAction = PictureAction::keep;
        break;

    case Action::pop:
        popBack();
        break;

    default:
        break;
    }


    m_prevAction = action;

    return picAction;
}


int Snake::getSpeed() const
{
    if (m_speed == stopped)
        return 0;

    return m_speed;
}


bool Snake::squareOverlaps(const Square& square, int skipLeading/* = 0*/) const
{
    for (int i = m_squares.size() - 1 - skipLeading; i >= 0; i--)
        if (square == m_squares.at(i))
           return true;

    return false;
}


bool Snake::shift(char key)
{
    do
    {
        for (auto& square : m_squares)
            square.move(key);
    }
    while (m_feeder->foodOverlaps(*this));

    return true;
}


bool Snake::move(char key)		//vraca bool, ci do seba narazil
{
    if (!m_squares.empty())
    {
        handleDirection(key);

        Square movedSquare = m_squares.back().movedSquare(m_direction);          //posun predny stvorec

        //musi ich byt aspon 5 => preskoc prve 4
        if (!m_canOverlap)
            if (squareOverlaps(movedSquare, 4))              //ci predny narazil
                return false;

        m_squares.push_back(movedSquare);                      //predny stvorec pridaj na pred

		for (auto i = m_squares.size() - 1; i > 0; i--)                 //prvemu daj farbu druheho atd.
			giveSquareColor(m_squares.at(i), m_squares.at(i - 1).m_iterColor);

        if (m_feeder->foodTaken(movedSquare))
            giveSquareColor(m_squares.front(), m_squares.at(1).m_iterColor - 1);
        else
            m_squares.pop_front();                                //vyhod posledny stvorec
    }

	return true;
}


void Snake::giveSquareColor(Square& square, int iterator)
{
    square.m_iterColor = iterator;

    square.m_color = m_colorManager.colorIterator.getAt(square.m_iterColor);
}

void Snake::draw() const
{
    for (auto& square : m_squares)
        square.draw(m_isSelected);
}

bool Snake::grow(char key)
{
    if (handleDirection(key) == DirectionAction::keyNotChanged)
        return false;

    Square movedSquare = m_squares.back().movedSquare(key);

    if (movedSquare == m_squares.back())
        return false;

    //if (chain.m_size() > 1 && (Square::smer != key || movedSquare == chain.at(chain.m_size()-2)))    //ak neda novy stvorec na seba, pridaj ho
    //if (m_squares.m_size() > 1 && Square::smer != key)    //ak neda novy stvorec na seba, pridaj ho
        //return false;

    addSquare(movedSquare);

    return true;
}


DirectionAction Snake::handleDirection(char key)
{
    DirectionAction dirAction = DirectionAction::keyIgnored;

    switch (key)
    {
    case control::left:
    case control::right:
    case control::up:
    case control::down:
        dirAction = m_direction != control::getOpposite(key) ? DirectionAction::keyChanged : DirectionAction::keyNotChanged;
        break;

    case -1:                             //stane sa pri pohybe hada s iba 1 stvorcom
        if (m_direction == 0)
        {
            m_direction = control::left;
            dirAction = DirectionAction::keyChanged;        //nasty hack
        }
        break;

    default:
        ;
    }

    if (dirAction == DirectionAction::keyChanged)
        m_direction = key;


    return dirAction;
}


bool Snake::isClicked(int x, int y) const
{
    Square::Corner point(x, y);

    for (auto& square : m_squares)
        if (square.containsPoint(point))
            return true;

    return false;
}

void Snake::popBack()
{
    if (m_squares.size() > 1)
        m_squares.pop_front();            //'zadny' prvok je uplne prvy v deque
}


void Snake::highlight(const cv::Scalar& color, size_t thickness/* = 4*/) const
{
	auto& colBlack = ColorManager::getColor(ColorManager::black);
	auto blackThickness = Square::getMargin() * 2 - 1;

	for (auto& square : m_squares)
		square.highlight(color, thickness + blackThickness);

	for (auto& square : m_squares)
		square.highlight(colBlack, blackThickness);
}


bool Snake::snakeOverlaps(const Snake& other) const
{
    for (auto& square : other.m_squares)
        if (squareOverlaps(square))
            return true;

    return false;
}


bool Snake::snakeOverlaps(const std::vector<Snake>& others) const
{
    for (auto& other : others)
        if (this != &other && snakeOverlaps(other))
            return true;

    return false;
}
