#include "snakegame.h"

enum
{
	stopped = 200,
	squaresInBoard = 50
};

SnakeGame::SnakeGame(size_t squareSize/* = 8*/, size_t squareMargin/* = 1*/)
	: m_idxSelected(m_snakes)
{	
	Square::setDimensions(squareSize, squareMargin);

	s_boardSize = squaresInBoard * Square::getTotalSize();

	m_pImg.create(s_boardSize, s_boardSize, CV_8UC3);
	cv::namedWindow("Graf");
	Square::setImage(m_pImg);

	m_feeder.snakes = &m_snakes;

	m_snakes.push_back(Snake(Square::Corner(100, 100), m_feeder, true));


	{
		lastSnake().m_feeder = &m_feeder;
		//m_feeder.addSnake(lastSnake());

		printf("snake at %p has feeder at %p\n", &lastSnake(), lastSnake().m_feeder);
	}
}


SnakeGame::~SnakeGame()
{
	cv::destroyWindow("Graf");	
}


size_t SnakeGame::getBoardSize()
{
	return s_boardSize;
}


Snake& SnakeGame::lastSnake()
{
	return m_snakes.back();
}


void SnakeGame::animate()
{
	/*
	 *IMAGE FLOW:
	 *prvotne = ukaz, vymaz
	 *potom = zober key, nakresli, ukaz, vymaz
	*/

	bool firstDraw = true;

	PictureAction picAction = PictureAction::redraw;

	while (true)
	{
		if (!firstDraw)
		{
			picAction = handleAction(cv::waitKey(0));

			if (picAction == PictureAction::finish)
				break;
		}

		firstDraw = false;

		if (picAction == PictureAction::redraw)
		{
			drawSnakes(true);
			imshow("Graf", m_pImg);
			m_pImg.setTo(0);
		}
	}

	m_bGrowing = false;
	picAction = PictureAction::redraw;
	char key = 0;

	while (true)
	{
		if (picAction == PictureAction::redraw)
		{
			m_feeder.drawFood();

			drawSnakes(false);

			imshow("Graf", m_pImg);
		}

		key = cv::waitKey(m_snakes[0].getSpeed());

		//picAction = snakes[0].handleAction(key);
		picAction = handleAction(key);

		if (picAction == PictureAction::finish)
			break;

		if (picAction == PictureAction::redraw)
			m_pImg.setTo(0);                               //vycierni vsetko
	}

	while (cv::waitKey(0) != control::finish);                              //caka na medzeru
}


Action SnakeGame::handleGrowing(char key)
{
	Action action = Action::none;


	switch (key)
	{
	case control::select:
		if (!m_bCloning && m_snakes.size() > 1)                      //oznacuje sa len pri viac hadoch
			action = Action::select;
		break;

	case control::clone:
		action = !m_bCloning ? Action::clone : Action::cloneKill;
		break;

	case control::cloneDone:
		if (m_bCloning && !m_snakes[m_idxSelected].snakeOverlaps(m_snakes))
			action = Action::cloneDone;
		break;

	case control::left:
	case control::right:
	case control::up:
	case control::down:
		//if (m_prevAction != Action::highlight)
		action = Action::grow;
		break;

	case control::moveLeft:
	case control::moveRight:
	case control::moveUp:
	case control::moveDown:
		action = Action::shift;
		break;

	case control::pop:
		action = Action::pop;
		break;

	case control::kill:
		action = Action::kill;
		break;

	default:
		;
	}

	return action;
}


Action SnakeGame::handleSpeed(char key)
{
	/*space - koniec hry
	 t:
		ismoving == true -> stop
		else -> resume

	 w a s d:
		ismoving = true -> zmen smer
		else -> posun o 1, zmen smer, ismoving ostava false

	ine:
		m_bMoving -> nic
		else -> posun o 1 v smere, ismoving ostava false
	 */

	if (m_bFirstStart)
	{
		m_bMoving = true;
		m_bFirstStart = false;
	}

	//bool moving = m_bMoving, advance = false;
	Action action = Action::none;


	switch (key)
	{
	case control::slower:
		if (m_speed < stopped)
			m_speed += 10;
		break;                                        //pridanie spomaluje

	case control::faster:
		if (m_speed > 10)
			m_speed -= 10;                            		//odobranie zrychluje
		break;

	case control::pause:
		if (m_bMoving)
		{
			m_speedPrevious = m_speed;
			m_speed = stopped;
		}
		else
			m_speed = m_speedPrevious;

		//moving = !m_bMoving;
		m_bMoving = !m_bMoving;
		break;


	case control::moveLeft:
	case control::moveRight:
	case control::moveUp:
	case control::moveDown:
		if (!m_bMoving)
			action = Action::shift;
		break;

	case control::pop:
		action = Action::pop;
		break;

	default:
		//advance = true;
		break;
	}

	//m_bMoving = moving;

//    if (action == Action::none)
//        action = (advance || m_bMoving && getSpeed() > 0) ? Action::move : Action::stay;       //vrati ci sa pohne

	return action;
}


PictureAction SnakeGame::handleAction(char key)
{
	//outputFile << key;

	if (key == control::finish)
		return PictureAction::finish;

	if (!m_bGrowing)
		return moveSnakes2(key);


	PictureAction picAction = PictureAction::redraw;

	Action action;// = isGrowing ? handleGrowing(key) : handleSpeed(key);

	action = handleGrowing(key);


	switch (action)
	{
		//GROW
	case Action::select:
		m_snakes[++m_idxSelected].highlight();
		break;

	case Action::clone:
		cloneStart();
		break;

	case Action::cloneKill:
		cloneKill();
		break;

	case Action::cloneDone:
		cloneDone();
		break;

	case Action::grow:
		if (!m_snakes[m_idxSelected].grow(key))
			picAction = PictureAction::keep;
		break;


		//NEUTRAL
	case Action::shift:
		if (!m_snakes[m_idxSelected].shift(key))
			picAction = PictureAction::keep;
		break;

	case Action::pop:
		m_snakes[m_idxSelected].popBack();
		break;

	case Action::kill:
		killSnake();
		break;
		//    //MOVE
		//    case Action::stay:
		//        picAction = PictureAction::keep;
		//        break;

		//    case Action::move:
		//        if (!moveSnakes(key))
		//            return PictureAction::finish;                               //vraca bool;narazil do seba=false

		//        if (m_feeder.isFeedingTime())
		//            m_feeder.placeFood();
		//        break;

	default:
		break;
	}

	//m_prevAction = action;

	return picAction;
}


PictureAction SnakeGame::moveSnakes2(char key)
{
	PictureAction picAction;
	bool redraw = false;

	for (auto& snake : m_snakes)
	{
		picAction = snake.handleMovement(key);

		if (picAction == PictureAction::finish)
			return picAction;

		if (picAction == PictureAction::redraw)
			redraw = true;
	}

	return redraw ? PictureAction::redraw : PictureAction::keep;
}


void SnakeGame::drawSnakes(bool drawSelect)
{
	auto snakesSize = m_snakes.size();

	int snakesToDraw = m_bCloning ? snakesSize - 1 : snakesSize;

	if (drawSelect && snakesToDraw > 1)
		m_snakes[m_idxSelected].highlight();
	
	for (auto& snake : m_snakes)
		snake.draw();
}


bool SnakeGame::moveSnakes(char key)
{
	bool result = false;

	for (auto& snake : m_snakes)
	{
		if (snake.handleSpeed(key) == Action::move)
			if (snake.move(key))
				result = true;
	}

	return result;
}


void SnakeGame::cloneStart()
{
	m_snakes.push_back(m_snakes[m_idxSelected]);

	m_bCloning = true;
	m_idxSelectedPrevious = m_idxSelected;
	m_idxSelected = m_snakes.size() - 1;           //pridany je posledny

	lastSnake().m_isSelected = true;
}


void SnakeGame::cloneKill()
{
	m_snakes.pop_back();
	m_idxSelected = m_idxSelectedPrevious;
	m_bCloning = false;
}


void SnakeGame::cloneDone()
{
	lastSnake().m_isSelected = false;
	m_bCloning = false;
}


void SnakeGame::killSnake()
{
	m_snakes.erase(m_snakes.begin() + m_idxSelected);
	m_idxSelected.clip();
}


size_t SnakeGame::s_boardSize;