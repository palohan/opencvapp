#pragma once

#include "square.h"
#include "utils.h"

enum class SnakeState : char {moving, growing, highlighted, selected};
enum class Action : char {move, stay, shift, pop, kill, grow, select, clone, cloneDone, cloneKill, none};

enum class PictureAction : char {redraw, keep, finish};

enum class DirectionAction : char {keyChanged, keyNotChanged, keyIgnored};

class Feeder;

class Snake
{
private:
    int m_speed = 100, m_speedPrevious;

    ColorManager m_colorManager;
    std::deque<Square> m_squares;
    bool m_canOverlap, m_bFirstStart = true;

    bool m_bMoving = false;
    Action m_prevAction = Action::none;

    Square::Corner m_prevPosition;

    char m_direction = 0;

public:
    bool m_isSelected = false;

    Feeder* m_feeder = nullptr;

    //Snake();
    //Snake(const Snake& other);
    Snake(const Square& s, Feeder &m_feeder, bool overlaps = false);
    ~Snake();

    void addSquare(const Square& s);

    Action handleSpeed(char key);
    Action handleGrowing(char key);
    PictureAction handleMovement(char key);


    int getSpeed() const;


    bool squareOverlaps(const Square& square, int skipLeading = 0) const;    
    bool snakeOverlaps(const Snake& other) const;
    bool snakeOverlaps(const std::vector<Snake>& others) const;


    Square::Corner getPosition() const;

    bool move(char smer);

    DirectionAction handleDirection(char key);

    void draw() const;

    bool grow(char key);

    void giveSquareColor(Square& square, int iterator);

    bool shift(char key);

    bool isClicked(int x, int y) const;

    void popBack();

    void highlight(const cv::Scalar& color = ColorManager::getColor(ColorManager::lightGreen), size_t thickness = 4) const;
};


