#include "square.h"
#include "colormanager.h"
#include "snakegame.h"

using namespace cv;


Square::Corner::Corner(int x, int y) : MyCvPoint(x, y)
{
    adjust();
}

Square::Corner::Corner(const MyCvPoint& p) : MyCvPoint(p)
{
    adjust();
}

Direction Square::Corner::directionToNeighbor(const Corner& other) const
{
    Plane plane = getSamePlane(other);

    if (plane == Plane::horizintal)
        return y < other.y ? Direction::right : Direction::left;

    if (plane == Plane::vertical)
        return x < other.x ? Direction::down : Direction::up;

    return Direction::none;
}

Plane Square::Corner::getSamePlane(const Corner& other) const
{
    if (x == other.x)
        return Plane::horizintal;

    if (y == other.y)
        return Plane::vertical;

    return Plane::none;
}

cv::Point Square::Corner::getBorderPoint(bool isLeft, size_t delta/* = 0*/) const
{
	double totalSize = s_nTotalSize;

    int borderX = round(x / totalSize) * totalSize;
    int borderY = round(y / totalSize) * totalSize;	

    if (delta < 4)
    {
        if (isLeft)
            delta = -1 * delta;

        borderX += delta;
        borderY += delta;
    }

    return cv::Point(borderX, borderY);
}

void Square::Corner::adjust()
{
    x = x - x % s_nTotalSize + s_nMargin;
    y = y - y % s_nTotalSize + s_nMargin;
}


//SQUARE
Square::Square() :  m_iterColor(-1)
{}

Square::Square(const Corner& corner) :  m_iterColor(-1)
{
    adjustPoints(corner);
}

bool Square::operator ==(const Square& other) const
{
     return m_cornerLeft.overlaps(other.m_cornerLeft) && m_cornerRight.overlaps(other.m_cornerRight);
}

MyCvPoint Square::center() const
{
    return MyCvPoint(m_cornerLeft.x + s_nTotalSize / 2, m_cornerLeft.y + s_nTotalSize / 2);
}

bool Square::overlapsOther(const Square &other) const
{
    MyCvPoint c1 = center(), c2 = other.center();

    return  (size_t)abs(c1.x - c2.x) < s_nTotalSize && 
            (size_t)abs(c1.y - c2.y) < s_nTotalSize;
}

void Square::draw(bool selected) const
{
    rectangle(*m_pImg, m_cornerLeft.getCvPoint(), m_cornerRight.getCvPoint(), selected ? ColorManager::getColor(ColorManager::white) : m_color, FILLED);
}


void Square::adjustPoints(const Corner& corner)
{
	m_cornerLeft = corner;

	m_cornerRight.x = m_cornerLeft.x + s_nSize;
	m_cornerRight.y = m_cornerLeft.y + s_nSize;
}


Plane Square::getSamePlane(const Square& other) const
{
    return m_cornerLeft.getSamePlane(other.m_cornerLeft);
}


bool Square::isInVicinityOfOther(const Square& other, int vicinity, int& distance) const
{
    RelativePosition position = relativePositionToOther(other);

    if (position.side == Side::none)
        return false;

    if (position.side == Side::down && s_nSmer != control::down)                             //su rovnako horizontalne -> vzdialenost je vertikalna
        return false;

    if (position.side == Side::up && s_nSmer != control::up)                             //su rovnako horizontalne -> vzdialenost je vertikalna
        return false;

    if (position.side == Side::left && s_nSmer != control::left)                             //su rovnako horizontalne -> vzdialenost je vertikalna
        return false;

    if (position.side == Side::right && s_nSmer != control::right)                             //su rovnako horizontalne -> vzdialenost je vertikalna
        return false;

    distance = position.distance;

    return position.distance < vicinity;
}



RelativePosition Square::relativePositionToOther(const Square& other) const
{
    RelativePosition position;    
    Plane plane = getSamePlane(other);

    if (plane == Plane::horizintal)                                     //su rovnako horizontalne -> vzdialenost je vertikalna
    {
        position.distance = m_cornerLeft.y - other.m_cornerLeft.y;

        position.side = position.distance < 0 ? Side::down : Side::up;
    }
    else if (plane == Plane::vertical)                               //su rovnako vertikalne -> vzdialenost je horizontalna
    {
        position.distance = m_cornerLeft.x - other.m_cornerLeft.x;

        position.side = position.distance < 0 ? Side::right : Side::left;
    }

    position.distance = abs(position.distance);

    return position;
}

void Square::highlight(const cv::Scalar& color, size_t thickness/* = 1*/) const
{
	cv::Point leftUp(m_cornerLeft.x - thickness, m_cornerLeft.y - thickness),
		rightDown(m_cornerRight.x + thickness, m_cornerRight.y + thickness);

	rectangle(*m_pImg, leftUp, rightDown, color, FILLED);
}

void Square::setImage(cv::Mat& image)
{
    m_pImg = &image;
}

size_t Square::getSize()
{
	return s_nSize;
}

size_t Square::getMargin() 
{
	return s_nMargin;
}

size_t Square::getTotalSize()
{
	return s_nTotalSize;
}

void Square::setDimensions(size_t size, size_t margin)
{
	s_nSize = size;
	s_nMargin = margin;
	
	s_nTotalSize = s_nSize + s_nMargin * 2;
}


void Square::move(char direction)
{
	switch (direction)
	{
	case control::left:
	case control::moveLeft:
		m_cornerLeft.x -= s_nTotalSize;
		m_cornerRight.x -= s_nTotalSize;

		if (m_cornerRight.x <= 0)          //pravy roh je na lavom okraji
		{
			m_cornerRight.x = SnakeGame::getBoardSize() - s_nMargin;      //pravy roh daj na pravy okraj
			m_cornerLeft.x = m_cornerRight.x - s_nSize;
		}
		break;

	case control::right:
	case control::moveRight:
		m_cornerLeft.x += s_nTotalSize;
		m_cornerRight.x += s_nTotalSize;

		if (m_cornerLeft.x >= (int)SnakeGame::getBoardSize())
		{
			m_cornerLeft.x = s_nMargin;
			m_cornerRight.x = m_cornerLeft.x + s_nSize;
		}
		break;

	case control::up:
	case control::moveUp:
		m_cornerLeft.y -= s_nTotalSize;
		m_cornerRight.y -= s_nTotalSize;

		if (m_cornerRight.y <= 0)
		{
			m_cornerRight.y = SnakeGame::getBoardSize() - s_nMargin;
			m_cornerLeft.y = m_cornerRight.y - s_nSize;
		}
		break;

	case control::down:
	case control::moveDown:
		m_cornerLeft.y += s_nTotalSize;
		m_cornerRight.y += s_nTotalSize;

		if (m_cornerLeft.y >= (int)SnakeGame::getBoardSize())
		{
			m_cornerLeft.y = s_nMargin;
			m_cornerRight.y = m_cornerLeft.y + s_nSize;
		}
		break;

	default:
		;
	}
}

//void Square::moveBack()
//{
//    move(static_cast<char>(opposite(charToDirection(lastDirection))));
//}

//void Square::handleDirection(char key) const
//{
//    switch (key)
//    {
//    case control::left:
//        if (smer != control::right)
//            smer = key;
//        break;

//    case control::right:
//        if (smer != control::left)
//            smer = key;
//        break;

//    case control::up:
//        if (smer != control::down)
//            smer = key;
//        break;

//    case control::down:
//        if (smer != control::up)
//            smer = key;
//        break;

//    case -1:                             //stane sa pri pohybe hada s iba 1 stvorcom
//        if (smer == 0)
//            smer = control::left;
//        break;

//    default:
//        break;
//    }
//}


Square Square::movedSquare(char key) const
{
     Square newSquare(*this);

     newSquare.move(key);

     return newSquare;
}


int Square::getColorIterator() const
{
    return m_iterColor;
}


void Square::giveColor(ColorManager& manager)
{
    m_color = manager.colorIterator.getNext();
    m_iterColor = manager.getColorIterator();
}


bool Square::containsPoint(const Corner& corner) const
{
    return m_cornerLeft.overlaps(corner);
}


int Square::s_nSmer = 0;                           //poc. smer
size_t Square::s_nSize;
size_t Square::s_nMargin;
size_t Square::s_nTotalSize;
cv::Mat* Square::m_pImg;
