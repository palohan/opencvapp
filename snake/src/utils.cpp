#include "utils.h"
#include "snakegame.h"


MyCvPoint::MyCvPoint(int x, int y)
 : cv::Point(x, y)
{}

bool MyCvPoint::overlaps(const MyCvPoint& other) const
{
	return x == other.x && y == other.y;
}

cv::Point MyCvPoint::getCvPoint() const
{
	return cv::Point(x, y);
}

MyCvPoint MyCvPoint::randomPoint()
{
	int randX = rand() % (SnakeGame::getBoardSize());		//TODO
	int randY = rand() % (SnakeGame::getBoardSize());

	return MyCvPoint(randX, randY);
}

Direction charToDirection(char key)
{
	return static_cast<Direction>(key);
}


control::control control::getOpposite(char ctrl)
{
	switch (ctrl)
	{
	case control::up:
		return control::down;
	case control::down:
		return control::up;
	case control::left:
		return control::right;
	case control::right:
		return control::left;
	}

	return control::none;
}


Direction opposite(Direction dir)
{
	auto chOpp = control::getOpposite(static_cast<char>(dir));

	return static_cast<Direction>(chOpp);
}
