#pragma once

#include "snake.h"
#include "feeder.h"
#include <fstream>

using namespace std;

class SnakeGame
{
    //ofstream outputFile;

    int m_speed, m_speedPrevious;

    bool m_bGrowing = true, 
		m_bCloning = false,
		m_bMoving, 
		m_bFirstStart;
    
	Feeder m_feeder;
    
	cv::Mat m_pImg;

    int m_idxSelectedPrevious;
	
    Action m_prevAction;
	
	vector<Snake> m_snakes;
	ContainerRangeCounter<decltype(m_snakes)> m_idxSelected;

	static size_t s_boardSize;
	
public:     
	SnakeGame(size_t squareSize = 8, size_t squarePadding = 1);
    ~SnakeGame();

    void addSnake(const Snake& snake);

    void grow();
    void move();
    void animate();

    Snake& lastSnake();

    void cloneStart();
    void cloneKill();
    void cloneDone();

    void killSnake();

	static size_t getBoardSize();


    //void setSelected(int sel);

    Action handleSpeed(char key);
    Action handleGrowing(char key);
    PictureAction handleAction(char key);

    void drawSnakes(bool drawSelect);

    bool moveSnakes(char key);
    PictureAction moveSnakes2(char key);
};


