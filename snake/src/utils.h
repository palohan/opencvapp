#pragma once

#include "colormanager.h"

namespace control
{
    enum control : char
    {
        left = 'a',
        right = 'd',
        up = 'w',
        down = 's',

        moveLeft = '4',
        moveRight = '6',
        moveUp = '8',
        moveDown = '2',

        faster = 'e',
        slower = 'r',
        pause = 't',

        pop = 'p',
        kill = 'k',

        select = 'h',
        clone = 'x',
        cloneDone = 'c',

        finish = ' ',
		none = 0
    };


	control getOpposite(char ctrl);
}


enum class Direction : char
{
	left = control::left,
	right = control::right,
	up = control::up,
	down = control::down,
	none = 0
};

Direction charToDirection(char key);
Direction opposite(Direction dir);


//class drawable
//{
//    cv::Mat* m_pImg;

//public:
//    setImage(cv::Mat* pImg)
//    {
//        m_pImg = pImg;
//    }
//};


class MyCvPoint : public cv::Point
{
public:
	MyCvPoint() = default;

	MyCvPoint(int x, int y);

	bool overlaps(const MyCvPoint& other) const;

	//    double distanceToOther(const MyCvPoint& other) const
	//    {
	//        return sqrt(pow(x - other.x, 2) + pow(y - other.y, 2));
	//    }

	cv::Point getCvPoint() const;

	static MyCvPoint randomPoint();
};