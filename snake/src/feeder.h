#pragma once

#include "square.h"
#include <vector>
#include "RangeCounter.h"

using namespace std;

class Snake;

class Feeder
{
private:
    typedef FixedRangeCounter<20> MoveIterator;
    MoveIterator feedInterval;

    vector<Square> m_foods;
    //vector<Snake*> snakes;
    ColorManager m_colorManager;

    static constexpr size_t maxFoods = 5;

    bool allFoodPlaced() const;

public:
    vector<Snake>* snakes;

    Feeder();

    bool isFeedingTime();

    void drawFood() const;

    void placeFood();

    bool foodTaken(const Square& movedSquare);

    //void addSnake(Snake& snake);

    bool foodOverlaps(const Snake& snake) const;
};


