#include "feeder.h"
#include "snake.h"
#include <stdio.h>

Feeder::Feeder() :
    feedInterval(0),
    snakes(0)
{}

//void Feeder::addSnake(Snake &snake)
//{
//    snakes->push_back(snake);
//}

bool Feeder::allFoodPlaced() const
{
    return m_foods.size() == Feeder::maxFoods;
}

bool Feeder::isFeedingTime()
{
    ++feedInterval;

    return !allFoodPlaced() && feedInterval.isMax();
}


void Feeder::drawFood() const
{
    for (auto& food : m_foods)
		food.draw();
}

void Feeder::placeFood()
{
    if (allFoodPlaced())
        return;

    Square sq;

    for (auto& snake : *snakes)
    {
        do
        {
            sq.adjustPoints(MyCvPoint::randomPoint());
        }
        while (snake.squareOverlaps(sq));              //nove jedlo nemoze lezat na hadoch
    }

    sq.giveColor(m_colorManager);
    sq.draw();

    m_foods.push_back(sq);
}


bool Feeder::foodOverlaps(const Snake& snake) const
{
	for (auto& food : m_foods)
        if (snake.squareOverlaps(food))
            return true;

    return false;
}


bool Feeder::foodTaken(const Square& movedSquare)
{
    if (!m_foods.size())
        return false;

    //int distance;
    //static size_t lastFood = -1;

	for (size_t i = 0; i < m_foods.size(); ++i)
	{
		if (movedSquare == m_foods[i])
		{
			m_foods.erase(m_foods.begin() + i);
			//lastFood = -1;
			feedInterval.reset();

			return true;
		}
		//else
		//    if (movedSquare.isNearOther(m_foods[i], 50, distance))
		//    {
		//        if (lastFood != i)
		//            putchar('\n');

		//        lastFood = i;

		//        printf("Snake is %d from m_foods[%d]\n", distance, i);
		//    }
	}

    return false;
}
