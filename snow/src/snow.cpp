#include "colormanager.h"

using namespace cv;

namespace snow
{
    constexpr auto winWidth = 1000;
    constexpr auto winHeight = 600;

    constexpr auto moveStepMin = 10;
    constexpr auto moveStepMax = 20;

    constexpr auto totalFrames = winHeight / moveStepMax;

    inline auto randMomveX() { return rand() % 60 - 30; }
    inline auto randMomveY() { return moveStepMin + rand() % moveStepMin; }

    constexpr auto maxFlakes = 60;
    constexpr auto wavesNo = 5;


    cv::Mat pimg;

    bool randColor = true;

    int frames = 0;

    class Flake
    {
        cv::Point point;
        ColorManager colManager;

    public:
        const cv::Scalar* clr;

        Flake()
        {
            reset();
        }

        void reset()
        {
			colManager.colorIterator = (rand() % colManager.colorIterator.getRange());

            clr = 0;
            point.y = 0;
            point.x = rand() % winWidth;
        }

        void move(int deltaX, int deltaY)
        {
            point.x += deltaX;
            point.y += deltaY;

            if (point.x < 0)
                point.x += winWidth;

            if (point.y >= winHeight)
                point.y -= winHeight;
        }

        void draw()
        {
            move(randMomveX(), randMomveY());
            cv::circle(pimg, point, 1, randColor ? colManager.colorIterator.getNext() : ColorManager::getColor(ColorManager::white), FILLED);
        }
    };


    int run(int argc, char *argv[])
    {
        pimg.create(winHeight, winWidth, CV_8UC3);

        Flake flakes[maxFlakes];


        int maxWave = 0, curFlakes;

        flakes[rand() % maxFlakes].clr = &ColorManager::getColor(ColorManager::red);

        char pressedKey;

        do
        {
            ++frames;

            if (maxWave < wavesNo)
                for (int i = 0; i < wavesNo; ++i)
                    if (frames >= totalFrames / wavesNo * i)
                        maxWave = i + 1;

            curFlakes = maxWave * maxFlakes / wavesNo;

            if (curFlakes < maxFlakes)
                printf("flakes: %d, frames %d\n", curFlakes, frames);

            pimg.setTo(0);

            for (int i = 0; i < curFlakes; ++i)
                flakes[i].draw();

            imshow("Graf", pimg);

            pressedKey = cv::waitKey(200);

            if (pressedKey == 'r')
                randColor = !randColor;
		}
		while (pressedKey != ' ');

        return 0;
    }
}

int main(int argc, char* argv[])
{
	return snow::run(argc, argv);
}
