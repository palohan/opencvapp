### What is this repository for? ###
Desperate college students & headhunters.

You are free to do anything with the code BUT make money. Money belong to me!

### Summary ###
These are 3 simple OpenCV projects from my student days. The code may not look that good and they are not maintained anymore.
    
1. **rockets** - point a rocket at the EVIL target and do damage!
2. **snake** - eat, sleep, sh!t and get BIG!
3. **snow** - watch colorful snow falling around - not even North Pole has it like this!


### How do I get set up? ###

First, you need OpenCV.

Next, use CMake out of source build to build all the projects. For example:


```
#!

mkdir build
cd build
cmake ..
make
```

For every project, a subdirectory will be created with the executable inside. Thus, to run snake, type


```
#!

snake/snake
```

NOTE: Changes to the folder structure require changing the CMakeLists.txt script.


Have fun!
