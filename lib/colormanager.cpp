#include "colormanager.h"


ColorManager::ColorManager() : colorIterator(m_colorsIterable)
{
}


int ColorManager::getColorIterator() const
{
    return colorIterator;
}


const std::array<const cv::Scalar, 6> ColorManager::m_colorsIterable =
{
    {
        {255,0,0},                                                      //blue
        {0,255,0},                                                      //green
        { 0,0,255 },                                                    //red
        {255,255,0},
        {0,255,255},
        {255,0,255}
    }
};

const cv::Scalar colors[] = { {255,255,255}, {111, 222}, 0 };


const cv::Scalar& ColorManager::getColor(ColorManager::color color)
{
	constexpr auto nonIterableSize = std::size(colors);

	if (color < nonIterableSize)
		return colors[color];

	return m_colorsIterable.at(color - 1);
}


//const std::vector<cv::Scalar>* ColorManager::ColorIterator::m_container = &ColorManager::m_colorsIterable;