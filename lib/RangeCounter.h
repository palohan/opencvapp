#pragma once

#include <iterator>


class RangeCounter
{
protected:
	int m_counter = 0;

	virtual size_t getRange() const = 0;

	int adjust(int it) const;

public:
	operator int() const;

	RangeCounter& operator= (int it);

	RangeCounter& operator++ ();                     // prefix: bez argumentu;
	RangeCounter& operator-- ();

	void reset();

	int max() const;

	bool isMax() const;

	void clip();
};


template<const int range>
class FixedRangeCounter : public RangeCounter
{
	static_assert(range > -1, "range must be a positive integer!");

    constexpr size_t getRange() const override
    {
		return range;
    }

public:
	FixedRangeCounter() = default;

    FixedRangeCounter(int it)
    {
        m_counter = adjust(it);
    }
};


template <class container>
class ContainerRangeCounter : public RangeCounter
{
	const container* m_container;			//using * so operator= is happy
	using elem_const_ref = typename container::const_reference;

public:
	size_t getRange() const override
	{
		return std::size(*m_container);
	}

	ContainerRangeCounter(const container& cont)
		: m_container(&cont)
	{
	}



	elem_const_ref getCurrent() const
	{
		return (*m_container).at(m_counter);
	}

	elem_const_ref getAt(int pos) const
	{
		pos = adjust(pos);
		return (*m_container).at(pos);
	}

	elem_const_ref getNext()
	{
		RangeCounter::operator++();
		return getCurrent();
	}

	elem_const_ref getPrev()
	{
		RangeCounter::operator--();
		return getCurrent();
	}

	elem_const_ref peekNext() const
	{
		return getAt(m_counter + 1);
	}

	elem_const_ref peekPrev() const
	{
		return getAt(m_counter - 1);
	}

	using RangeCounter::operator=;
};
