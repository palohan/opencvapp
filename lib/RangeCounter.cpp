#include "RangeCounter.h"


int RangeCounter::adjust(int it) const
{
	it %= getRange();

	if (it < 0)
		it += getRange();

	return it;
}


RangeCounter::operator int() const
{
	return m_counter;
}

RangeCounter& RangeCounter::operator= (int it)
{
	m_counter = adjust(it);
	return *this;
}


RangeCounter& RangeCounter::operator++ ()                     // prefix: bez argumentu
{
	m_counter = adjust(++m_counter);
	return *this;
}

RangeCounter& RangeCounter::operator-- ()
{
	m_counter = adjust(--m_counter);
	return *this;
}


void RangeCounter::reset()
{
	m_counter = 0;
}

int RangeCounter::max() const
{
	return getRange() - 1;
}

bool RangeCounter::isMax() const
{
	return m_counter == max();
}

void RangeCounter::clip()
{
	if (m_counter < 0)
		m_counter = 0;
	else if ((size_t)m_counter >= getRange())
		m_counter = max();
}