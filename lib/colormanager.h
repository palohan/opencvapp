#pragma once

#include "RangeCounter.h"

#include <opencv2/opencv.hpp>
#include <vector>


class ColorManager
{
	static const std::array<const cv::Scalar, 6> m_colorsIterable;

public:
	enum color { white, lightGreen, black, blue, green, red,  };

    typedef ContainerRangeCounter<decltype(m_colorsIterable)> ColorIterator;
    ColorIterator colorIterator;

	static const cv::Scalar& getColor(color name);

public:
    ColorManager();

    int getColorIterator() const;
};